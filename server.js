const Hapi = require('@hapi/hapi')
const Config = require('./src/config')
const log = require('./src/utils/log')

const server = new Hapi.Server(Config.server.connection)

server.ext('onRequest', function (request, h) {
  log.info(`Request: ${request.method}, ${request.path}`);
  return h.continue;
});

const init = async () => {
  try {
    await server.register(Config.server.registers)

    await server.start()
    log.info(`Server is running at ${server.info.uri}`);
  } catch (error) {
    log.error(`Server error: ${error}`);
  }
}
init()

module.exports = server
