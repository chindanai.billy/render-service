const Controller = require("./api.controller");

const routes = [
  {
    method: "POST",
    path: "/render-service/auth-user",
    config: Controller.authenUser,
  },
  {
    method: "POST",
    path: "/render-service/auth-customer",
    config: Controller.authenCustomer,
  },
  {
    method: "POST",
    path: "/render-service/register-customer",
    config: Controller.registerCustomer,
  },
  {
    method: "GET",
    path: "/render-service/get-group-fe",
    config: Controller.getGroupFe,
  },
  {
    method: "POST",
    path: "/render-service/get-category-by-id",
    config: Controller.getCategoryByIdFe,
  },
  {
    method: "GET",
    path: "/render-service/get-product-fe",
    config: Controller.getProductFe,
  },
  {
    method: "GET",
    path: "/render-service/get-new-arrival",
    config: Controller.getNewArrival,
  },
  {
    method: "GET",
    path: "/render-service/get-collection-fe",
    config: Controller.getCollectionFe,
  },
  {
    method: "POST",
    path: "/render-service/get-collection-by-id-fe",
    config: Controller.getCollectionByIdFe,
  },
  {
    method: "GET",
    path: "/render-service/get-modal-home-fe",
    config: Controller.getModalHomeFe,
  },
  {
    method: "GET",
    path: "/render-service/get-home-fe",
    config: Controller.getHomeFe,
  },
  {
    method: "GET",
    path: "/render-service/get-blog-fe",
    config: Controller.getBlogFe,
  },
  {
    method: "GET",
    path: "/render-service/get-blog-by-id-fe/{id}",
    config: Controller.getBlogByIdFe,
  },
  {
    method: "GET",
    path: "/render-service/get-about-fe",
    config: Controller.getAboutFe,
  },
  {
    method: "GET",
    path: "/render-service/get-campaign-fe",
    config: Controller.getCampaignFe,
  },
  
];

module.exports = routes;
