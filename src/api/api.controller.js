const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const Boom = require("@hapi/boom");
const _ = require("underscore");
const Constant = require("../constant");
const Handler = require("./api.handler");
const log = require("../utils/log");
const jwt = require("jsonwebtoken");
const Response = require("../utils/response");

const authenUser = {
  auth: false,
  //...ApiValidate.authen,
  handler: async (request) => {
    try {
      const payload = request.payload;
      log.debug("Payload: %o", payload);

      const { username = "", password = "" } = payload.data;

      let authFail = false;
      let jwtPayload = {};

      const resultPassword = await Handler.authenPasswordUser(
        username,
        password,
        prisma
      );

      authFail = resultPassword.authFail;
      jwtPayload = resultPassword.jwtPayload;

      if (authFail) {
        return Response.authFail(username);
      }

      log.debug("JWT data: %o", jwtPayload);

      const key = new Buffer.from(Constant.JWT_SECRET, "base64");
      const token = jwt.sign(jwtPayload, key, {
        algorithm: "HS256",
        expiresIn: 12 + "h",
        header: { TYP: "JWT" },
      });

      let decoce = jwt.decode(token);

      return {
        statusCode: 200,
        result: {
          authToken: token,
          id: jwtPayload.id,
          exp: decoce.exp,
        },
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const authenCustomer = {
  auth: false,
  //...ApiValidate.authen,
  handler: async (request) => {
    try {
      const payload = request.payload;
      log.debug("Payload: %o", payload);

      const { username = "", password = "" } = payload.data;

      let authFail = false;
      let jwtPayload = {};

      const resultPassword = await Handler.authenPasswordCustomer(
        username,
        password,
        prisma
      );

      authFail = resultPassword.authFail;
      jwtPayload = resultPassword.jwtPayload;

      if (authFail) {
        return Response.errorMsg(401, jwtPayload);
      }

      log.debug("JWT data: %o", jwtPayload);

      const key = new Buffer.from(Constant.JWT_SECRET, "base64");
      const token = jwt.sign(jwtPayload, key, {
        algorithm: "HS256",
        expiresIn: 12 + "h",
        header: { TYP: "JWT" },
      });

      let decoce = jwt.decode(token);

      return {
        statusCode: 200,
        result: {
          authToken: token,
          id: jwtPayload.id,
          exp: decoce.exp,
        },
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const registerCustomer = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;

      const resultData = await Handler.createCustomer(data, prisma);
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getEntityProfile = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;
      const { id } = payload.data;

      const { authValid, authData } = await Authentication.validate(
        request.headers,
        Constant.JWT_SECRET
      );
      if (!authValid) {
        return Response.unauthorizedError();
      }

      const getEntityProfile = await prisma.EntityProfiles.findUnique({
        where: {
          id: id,
        },
      });

      return {
        statusCode: 200,
        result: getEntityProfile,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getGroupFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getGroup = await prisma.GroupTable.findMany({
        orderBy: {
          created_at: "desc",
        },
        where: {
          status: true,
        },
        select: {
          id: true,
          group_name: true,
          group_description: true,
          status: true,
          CategoryTable: {
            where: {
              status: true,
            },
            select: {
              id: true,
              category_name: true,
              category_description: true,
              status: true,
              show_image: true,
              hover_image: true,
              ProductTable: {
                where: {
                  status: true,
                },
                select: {
                  id: true,
                  ref_id: true,
                  product_name: true,
                  product_description: true,
                  collection_table_id: true,
                  price: true,
                  size_fit: true,
                  weight: true,
                  detail: true,
                  care_guide: true,
                  show_image: true,
                  hover_image: true,
                  color: true,
                  new_arrival: true,
                  pre_order: true,
                  status: true,
                },
              },
            },
          },
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getGroup,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getCategoryByIdFe = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;

      if (typeof data.id !== "string") {
        const getCategory = await prisma.CategoryTable.findMany({
          orderBy: {
            created_at: "desc",
          },
          where: {
            id: +data.id,
          },
          select: {
            id: true,
            ProductTable: {
              where: {
                status: true,
              },
              select: {
                id: true,
                ref_id: true,
                product_name: true,
                product_description: true,
                collection_table_id: true,
                price: true,
                size_fit: true,
                weight: true,
                detail: true,
                care_guide: true,
                show_image: true,
                hover_image: true,
                color: true,
                new_arrival: true,
                pre_order: true,
                status: true,
              },
            },
          },
        });

        return {
          statusCode: 200,
          result: getCategory,
        };
      } else {
        const getCategory = await prisma.GroupTable.findMany({
          orderBy: {
            created_at: "desc",
          },
          where: {
            group_name: data.id,
          },
          select: {
            CategoryTable: {
              select: {
                id: true,
                category_name: true,
                category_description: true,
                show_image: true,
                status: true,
                hover_image: true,
                ProductTable: {
                  where: {
                    status: true,
                  },
                  select: {
                    id: true,
                    ref_id: true,
                    product_name: true,
                    product_description: true,
                    collection_table_id: true,
                    price: true,
                    size_fit: true,
                    weight: true,
                    detail: true,
                    care_guide: true,
                    show_image: true,
                    hover_image: true,
                    color: true,
                    new_arrival: true,
                    pre_order: true,
                    status: true,
                  },
                },
              },
            },
          },
        });
        let product = [];
        for (let data of getCategory[0].CategoryTable) {
          product.push(...data.ProductTable);
        }

        let result = [];
        result.push({ id: 0, ProductTable: product });
        return {
          statusCode: 200,
          result: result,
        };
      }
      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getProductFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getProduct = await prisma.ProductTable.findMany({
        where: {
          status: true,
        },
        orderBy: {
          created_at: "desc",
        },
        select: {
          id: true,
          ref_id: true,
          product_name: true,
          product_description: true,
          collection_table_id: true,
          price: true,
          size_fit: true,
          weight: true,
          detail: true,
          care_guide: true,
          show_image: true,
          hover_image: true,
          color: true,
          new_arrival: true,
          pre_order: true,
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getProduct,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getNewArrival = {
  auth: false,
  handler: async (request) => {
    try {
      const getNewArrival = await prisma.ProductTable.findMany({
        orderBy: {
          created_at: "desc",
        },
        where: {
          new_arrival: true,
        },
        select: {
          id: true,
          product_name: true,
          product_description: true,
          collection_table_id: true,
          price: true,
          size_fit: true,
          weight: true,
          detail: true,
          care_guide: true,
          show_image: true,
          hover_image: true,
          color: true,
          new_arrival: true,
          pre_order: true,
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getNewArrival,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getCollectionFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getCollection = await prisma.CollectionTable.findMany({
        where: {
          status: true,
        },
        orderBy: {
          created_at: "desc",
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getCollection,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getCollectionByIdFe = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;

      const getCollection = await prisma.CollectionTable.findFirst({
        orderBy: {
          created_at: "desc",
        },
        where: {
          id: +data.id,
        },
      });

      let findItem = [];
      for (const item of getCollection.collection_item) {
        const getProduct = await prisma.ProductTable.findFirst({
          orderBy: {
            created_at: "desc",
          },
          where: {
            id: +item.id,
            status: true,
          },
          select: {
            id: true,
            product_name: true,
            product_description: true,
            collection_table_id: true,
            price: true,
            size_fit: true,
            weight: true,
            detail: true,
            care_guide: true,
            show_image: true,
            hover_image: true,
            color: true,
            new_arrival: true,
            pre_order: true,
            status: true,
          },
        });

        findItem.push(getProduct);
      }

      getCollection.collection_item = findItem;
      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getCollection,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getModalHomeFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getModalHome = await prisma.ModalHomeTable.findFirst({
        where: {
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getModalHome,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getHomeFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getHome = await prisma.HomeTable.findFirst({
        where: {
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getHome,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getBlogFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getBlog = await prisma.BlogTable.findMany({
        where: {
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getBlog,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getBlogByIdFe = {
  auth: false,
  handler: async (request) => {
    try {
      const { id } = request.params;

      const getBlog = await prisma.BlogTable.findFirst({
        where: {
          id: +id,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getBlog,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getAboutFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getAbout = await prisma.AboutTable.findFirst({
        where: {
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getAbout,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getCampaignFe = {
  auth: false,
  handler: async (request) => {
    try {
      const getCampaign = await prisma.CampaignTable.findMany({
        orderBy: {
          created_at: 'desc'
        },
        where: {
          status: true,
        },
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: getCampaign,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

module.exports = {
  authenUser,
  authenCustomer,
  registerCustomer,
  getGroupFe,
  getCategoryByIdFe,
  getProductFe,
  getNewArrival,
  getCollectionFe,
  getCollectionByIdFe,
  getModalHomeFe,
  getHomeFe,
  getBlogFe,
  getBlogByIdFe,
  getAboutFe,
  getCampaignFe,
};
