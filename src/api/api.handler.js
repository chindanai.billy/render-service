const bcrypt = require("bcrypt");
const _ = require("underscore");

const authenPasswordUser = (username, password, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userPassword = await prisma.UserTable.findFirst({
        where: {
          user_name: username,
        },
      });

      let authFail = true;
      let jwtPayload = {};

      const match = await bcrypt.compare(password, userPassword.password);
      if (match) {
        authFail = false;
        jwtPayload = { id: userPassword.id };
        console.log("login complete");
      } else {
        console.log("login fail");
      }

      resolve({
        authFail,
        jwtPayload,
      });
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const authenPasswordCustomer = (username, password, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const customerPassword = await prisma.CustomerTable.findFirst({
        where: {
          user_name: username,
        },
      });

      if (_.isEmpty(customerPassword)) {
        return resolve({
          authFail: true,
          jwtPayload: { msg: "Incorrect username or password." },
        });
      }

      let authFail = true;
      let jwtPayload = {};

      const match = await bcrypt.compare(password, customerPassword.password);
      if (match) {
        authFail = false;
        jwtPayload = { id: customerPassword.id };
        console.log("login complete");
      } else {
        console.log("login fail");
      }

      resolve({
        authFail,
        jwtPayload,
      });
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const createCustomer = (data, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionCreateCustomer = async () => {
        return await prisma.$transaction(async (tx) => {
          if (!_.isEmpty(data)) {
            return resolve({
              statusCode: 500,
              result: "Please fill in information",
            })
          }

          const checkDuplicate = await prisma.CustomerTable.findFirst({
            where: {
              user_name: data.user_name,
            },
          });

          if (!_.isEmpty(checkDuplicate)) {
            return resolve({
              statusCode: 500,
              result: "Duplicate",
            });
          }

          const salt = await bcrypt.genSalt(10);
          const hash = await bcrypt.hash(data.password, salt);

          const createCustomer = await tx.CustomerTable.create({
            data: {
              user_name: data.user_name,
              password: hash,
              status: true,
            },
          });

          await tx.PayerInformationTable.create({
            data: {
              customer_id: createCustomer.id,
              email: data.email,
              country: "thailand",
              first_name: data.first_name,
              last_name: data.last_name,
              address: data.address,
              optional: data.optional,
              subdistrict: data.subdistrict,
              city: data.city,
              province: data.province,
              postal_code: data.postal_code,
              phone: data.phone,
            },
          });

          if (!_.isEmpty(createCustomer)) {
            const getCustomer = await tx.CustomerTable.findMany({
              orderBy: {
                created_at: "desc",
              },
              select: {
                id: true,
                user_name: true,
                password: true,
                status: true,
                payer_information_table: {
                  select: {
                    id: true,
                    email: true,
                    country: true,
                    first_name: true,
                    last_name: true,
                    address: true,
                    optional: true,
                    subdistrict: true,
                    city: true,
                    province: true,
                    postal_code: true,
                    phone: true,
                  },
                },
              },
            });

            return resolve({
              statusCode: 200,
              result: getCustomer,
            });
          }
        });
      };

      await transactionCreateCustomer();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

module.exports = {
  authenPasswordUser,
  authenPasswordCustomer,
  createCustomer,
};
