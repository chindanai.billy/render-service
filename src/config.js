const vision = require('vision')
const inert = require('@hapi/inert')
const Constant = require('./constant')
const route = require('./route')
const prismaPlugin = require('./plugins/prisma')

module.exports = {
  server: {
    connection: {
      host: Constant.HOST || '0.0.0.0',
      port: Constant.PORT || 8000,
      routes: {
        cors: {
          origin: ['*'],
          credentials: true,
          additionalHeaders: ['Access-Control-Allow-Origin', 'Access-Control-Request-Method', 'Access-Control-Allow-Methods', 'language', 'network']
        }
      }
    },
    registers: [
      vision,
      inert,
      prismaPlugin,
      route,
      {
        plugin: require('hapi-alive'),
        options: {
          path: '/api/entity/health',
          tags: ['health', 'monitor'],
          responses: {
            healthy: {
              message: 'I\'m healthy!!!'
            },
            unhealthy: {
              statusCode: 400
            }
          },
          healthCheck: async function (server) {
            if (!server.info) {
              throw new Error('Server not healthy');
            }
            console.log('healthCheck => OK');
            return await true;
          }
        }
      }
    ]
  }
}
