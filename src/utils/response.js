const errorMsg = (status, msg) => {
  return {
    statusCode: status,
    result: msg,
  };
};

module.exports = {
  errorMsg,
};
