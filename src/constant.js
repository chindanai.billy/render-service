require("dotenv").config();

const Constant = {
  HOST: process.env.HOST,
  PORT: process.env.PORT,
  JWT_SECRET: process.env.JWT_SECRET_KEY,
};

module.exports = Constant;
